"""
    Немного функций и import-ов
    Необходимость функций не доказана
    Файл тут временно, но это не точно
"""

from random import seed, randint


# Генеририм случайное число  0 <= x < to
# Удобно, когда выбираем случайный элемент из массива
def rng(to=10):
    return randint(0, to-1)


# Складывает поэлементно два кортежа по три элемента
def addTuple3(left, right):
    a, b, c = left
    x, y, z = right
    return a+x, b+y, c+z


# Сложение rgb цветов с учетом верхней границы в 255 и нижней в 0
def addColors_(one, another):
    rgb = list(addTuple3(one, another))
    res = []
    for c in rgb:
        res.append((c if c >= 0 else 0) if c < 256 else 255)
    return tuple(res)


def addColors(one, another):
    r, g, b = addTuple3(one, another)
    r = (r if r >= 0 else 0) if r < 256 else 255
    g = (g if g >= 0 else 0) if g < 256 else 255
    b = (b if b >= 0 else 0) if b < 256 else 255

    return r, g, b




