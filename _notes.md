# Заметки
Тут кратко о симуляции

## Что есть клетка

Клетка - ИИ-управляемый персонаж, основное действующее лицо симуляции. Клетки живут на поле, питаются органикой/минералами/солнечным светом, размножаются делением или почкованием и меняют цвет в зависимости от рациона

Обладают геномом из N генов, которые определяют поведение клетки: геном является своеобразной программой, где каждый ген кодирует действие или условный/безусловный переход

Каждый ход указатель проходит по геному, пока не встретит "завершающее" действие (например шаг) или пока не достигнет лимита действий за ход. Действия расходуют энергию, переходы нет, но увеличивают значение счетчика действий

Каждое действие (кроме питания) затрачивает определенное количество энергии. В зависимости от результат действия клетка может перейти к следующему гену или пропустить определенное количество генов (условный переход)

Клетка может поворачиваться в одном из восьми направлений или совершать действие в направлении взгляда

Карта направлений:
- 7 0 1
- 6 К 2
- 5 4 3

По достижении максимума энергии или соответствуещего гена клетка отпочковывает потомка в одну из свободных прилегающих клеток. В зависимости от вида деления потомок получается свободным или прикрепленным к родителю. Если у клетки уже есть прикрепленный потомок, то она может создавать только свободных. 
Если у клетки не хватает энергии на размножение, то она погибает. Если недостаточно места для размножения, то клетка погибает от переизбытка энергии.

## Расшифровка генома
    
Гены в геноме бывают трёх видов:

**Действия:**

Отвечают за поведение клетки. После завершающих действий **[!]** клетка завершает свой ход

- 42 - **[!]** фотосинтез: клетка поглощает солненчную энергию
- 43 - **[!]** хемосинтез: клетка перерабатывает минералы в энергию
- 45 - относительный поворот: делает поворот на определенное количество пунктов (по часовой стрелке) в зависимости от следующего гена
- 46 - абсолютный поворот: делает поворот в определенную сторону в зависимости от следующего гена
- 38 - **[!]** шаг: сделать шаг в направлении взгляда, если возможно
- 40 - **[!]** укус: сделать укус в направлении взгляда, если возможно
- 62 - **[!]** деление: создать потомка если есть такая возможность*, потомок становится свободной клеткой
- 63 - **[!]** почкование: создать потомка если есть такая возможность*, потомок связан с родительской клеткой

*Потомок создастся в случайной свободной позиции из прилегающих

**Условные переходы:**

Отвечают за логику клетки. Клетка проверяет какое либо условие и выбирает следующий ген в зависимости от результата

- 55 - На что я смотрю: переход в зависимости от объекта перед клеткой
- 56 - Окружен ли я?
- 59 - Много ли у меня энергии?
- 60 - Много ли у меня минералов?
- 32 - Солнечно ли здесь?
- 31 - Есть ли минералы рядом?

**Безусловные переходы:**

Так-называемое бессознательное. Переходы, которые не зависят от окружения и состояния клетки. Указатель просто смещается на значение гена. Это все остальные гены.

## Интересные сиды

Вероятно, уже не работают

0.7705764775632411 100х200, 0.4 .5 - клетка-тихоходка, подъедающая все трупики

0.39575386601807916 100x200, 0.4 .9 - падальщик-быстроход

0.1428896805379819

