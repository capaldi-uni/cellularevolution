import pygame
import model
from artist import Artist
from random import seed, random
pygame.init()


def main():
    # Параметры запуска
    FPS = 30    # Скорость (кадры в секунду)
    size = 6    # Размер изображения клетки
    # Параметры поля
    W = 120
    H = 100
    border = True

    clock = pygame.time.Clock()

    # Инициализация генератора случайных чисел
    sd = model.GENSEED

    if sd is None:  # Если не задано зерно, то создаем новое случайно
        seed()
        sd = random()

    seed(sd)

    # Создание основных сущностей
    f = model.Field(H, W)   # Поле действий
    sc = pygame.display.set_mode((W * size, H * size))  # Поверхность для отрисовки
    a = Artist(sc, f, size, border)     # Художник, который будет рисовать поле на экране

    # Параметры воспроизведения
    pause = False
    speed = 1

    while True:     # Основной цикл

        # Обработка событий
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                pygame.quit()
                return
            if event.type == pygame.KEYDOWN:
                # Пауза
                if event.key == pygame.K_SPACE:
                    if pause:
                        pause = False
                    else:
                        pause = True
                # Переключение скоростей
                elif event.key == pygame.K_1:
                    speed = 1
                elif event.key == pygame.K_2:
                    speed = 2
                elif event.key == pygame.K_3:
                    speed = 3
                elif event.key == pygame.K_4:
                    speed = 4
                elif event.key == pygame.K_5:
                    speed = 5
                elif event.key == pygame.K_6:
                    speed = 6
                elif event.key == pygame.K_0:
                    speed = 16
                # Вывод зерна генератора
                elif event.key == pygame.K_s:
                    print(sd)
                # Вывод текущего хода
                elif event.key == pygame.K_t:
                    print("Current turn is", f.turn)

        if not pause:
            for _ in range(speed):  # Выполняем несколько ходов в зависимости от скорости
                f.hostTurn()

            # Отрисовываем поле и обновляем экран
            a.draw()
            pygame.display.update()

        clock.tick(FPS)


if __name__ == '__main__':
    main()
